# read file as list
def file_to_list(file):
    lines = []
    # open file
    with open(file) as f:
        for line in f:
            # remove \n character
            lines.append(line[:-1])
    
    return lines

# read file as one string
def file_to_string(file):
    file_string = ""
    
    # open file
    with open(file) as f:
        for line in f:
            # remove \n character
            file_string = file_string + line[:-1]
    
    return file_string


# print to file in single line
def fprints(content, file = "test", file_mode = "w"):    
    # open file
    with open(file, file_mode) as f:
        f.write(content)
    
    print(f"Printed to file {file}.")


# print list to file
def fprintl(content, file = "test"):
    # count lines
    lc = 0

    # open file
    with open(file, "w") as f:
        # go through each line and print
        for line in content:
            if lc == 0:
                f.write(line)
            else:
                f.write("\n" + line)
        
            lc += 1
    
    print(f"Printed to file {file}.")


# print list line by line
def printl(list):
    for l in list:
        print(l)


# print dictionary line by line
def printd(dict):
    for l in dict:
        print()
        print(dict[l])
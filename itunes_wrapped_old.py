from python_cheats import printl, file_to_string, printlf, printd
import operator


def xml_to_list(xml_st):
    # create list to store xml dictionaries
    element_l  = []

    # clean up string by removing spaces and splitting by tags
    xml_st = xml_st.replace('\t', '')
    xml_l = xml_st.split("><")

    # convert string to list of elements
    for e in xml_l:
        # use tag symbols to separate tag from value (if value is attached)
        e_value = ""
        e = e.split(">")
        e_name = e[0]

        # account for no values
        if len(e) > 1:
            e_value = e[1].split("<")[0]

        # add tag and value to new array
        element_l.append([e_name, e_value])
    
    return element_l
        

def create_song_library_dict(xml):
    # create element list from xml and initialize other variables
    element_l = xml_to_list(xml)
    song_library = {}
    song_data = {}
    ct = 0
    new_track = False

    # check each element list item until the song data section is over
    for e in element_l:
        # check for end of song data
        if "key" in e and "Playlists" in e:
            break

        # check for track key
        if "Track ID" in e:
            # get value that matches track id
            track_id = element_l[ct + 1][1]
            new_track = True

        # continue to get song_data until dict closing tag
        if new_track:
            # if end of song_data
            if "/dict" in e:
                new_track = False
                song_library[track_id] = song_data
                song_data = {}
                continue

            # if another key is found, add key and value to song_data dict
            if "key" in e:
                key = element_l[ct][1]
                value = element_l[ct + 1][1]
                song_data[key] = value

        ct += 1

    return song_library


# takes difference between the two library play counts and gives total play count for the year
def get_yr_pc(library_s_raw, library_e_raw):
    # only add songs with play counts
    library_s = {}
    library_e = {}

    # only add songs with play counts
    for song in library_s_raw:
        if "Play Count" in library_s_raw[song]:
            library_s[song] = library_s_raw[song]
    for song in library_e_raw:
        if "Play Count" in library_e_raw[song]:
           library_e[song] = library_e_raw[song]

    # initialize total library
    total_library = library_e

    # go song by song with newest library
    for song in library_e:
        # set pc for new songs not in starting library
        year_pc = 0
        if song not in library_s:
            year_pc = int(library_e[song]["Play Count"])

        # calculate play count changes
        else:         
            s_pc = int(library_s[song]["Play Count"])
            e_pc = int(library_e[song]["Play Count"])
            year_pc = e_pc - s_pc

        total_library[song]["One Year Play Count"] = year_pc

    # document removed songs
    removed_songs = {}
    for song in library_s:
        # only add if missing from total library
        if song not in total_library:
            removed_songs[song] = library_s[song]

    return sort_library(total_library)


def sort_library(library):
    yr_pc_songs = []
    sorted_library = {}

    for song in library:
        if "Name" in library[song]:
            yr_pc_songs.append([song, library[song]["One Year Play Count"]])

    yr_pc_songs = sorted(yr_pc_songs, key=lambda x: x[1])

    # add back to list
    for item in yr_pc_songs:
        song = item[0]

        sorted_library[song] = library[song]
    
    return sorted_library



    


def main():
    xml_s = file_to_string("itunes_xmls/LibraryNov2021.xml")
    xml_e = file_to_string("itunes_xmls/LibraryDec2022.xml")

    library_s = create_song_library_dict(xml_s)
    library_e = create_song_library_dict(xml_e)

    results = get_yr_pc(library_s, library_e)

    printd(results)

    

main()
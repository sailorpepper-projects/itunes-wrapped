# iTunes Wrapped

## Files

## Requirements
The following are required to use this program:
- iTunes library xml files from the beginning and end of the year.
- Probably Python 3.6.8 or newer.
- Probably poetry

## Overview
This program is designed to read iTunes library data and tell the user their top songs and artists of the year. It is meant to mimic the Spotify "Wrapped" feature.

## Description
This program does the following:
1. Takes two iTunes library xmls and adds the song information to a dictionary.
1. Compares the two dictionaries.
1. Creates a new dictionary with the added value: One Year Play Count
1. Returns top plays for the year.

## Installation
To install, do the following
1. Clone this project to the directory of your choice.
1. Create a directory called `itunes_xmls`.
1. Copy your two library files to the itunes_xmls directory.
1. Title your year start library file `library_start.xml`.
1. Title your year end library file `library_end.xml`.
1. Do something with poetry for installing stuff. Or install stuff it in the environment of your choice.
1. In the main project directory, run the python project.